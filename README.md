## Use
Simply clone this repository, change your directory to it, and run `./gamestop.sh`. `gamestop-after-hours.sh` should work to give you the after-hours price.

This could pretty obviously be repurposed to track any other stock. Simply change "GME" in the Yahoo Finance URL to another stock.
